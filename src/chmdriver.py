from pscotton import *
import os


p = CHMProject('example','test.chm')

p.load_files()

p.generate_toc()
p.generate_index()

p.generate_hfp()

p.chmcmd()
