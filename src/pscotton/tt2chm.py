# preshrunk-cotton
# Copyright (C) 2010, 2011, 2014, 2015 Approximatrix, LLC
# http://code.google.com/p/preshrunk-cotton/
# http://approximatrix.com/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# A full version of the license terms is available in LICENSE.

from pscotton.textproc import *
from pscotton.chm import *
import os.path

COMMON_HEADINGS = ['See Also','Introduction','Conclusion','Description','Examples',
                   'References','Links']

def tt2chm(src, dest=None, ignore_strings=COMMON_HEADINGS,
           chm_index=True, chm_toc=True, first_page=None,
           title=None, tocspec=None, specified_chmfile=None,
           header_index_depth=3, toc_internal_name=None):
    """Reads the src directory tree, converts all textile files (must be
    marked with the extension '.textile') to html, and copies all remaining
    files to the destination.  The destination directory is then rewalked
    and converted into a compressed HTML Help file.

    This function requires that 'chmcmd' program is available on the
    current path.  'chmcmd' is part of Free Pascal and the associated
    Lazarus IDE (http://www.freepascal.org/).
    """

    if dest == None:
        dest = src

    # Process textile
    processor = TextileProcessor(src, dest)
    processor.to_html()

    # Perform CHM tasks
    chmfile = specified_chmfile
    if chmfile is None:
        chmfile = os.path.basename(dest)+'.chm'
        
    compressor = CHMProject(dest, chmfile)
    
    # Set the internal TOC filename if specified
    if toc_internal_name is not None:
        compressor.tocfilename = toc_internal_name

    # Build CHM index
    if chm_index:
        compressor.generate_index(ignore_strings, header_index_depth=header_index_depth)

    # Build the table of contents
    if chm_toc:
        compressor.generate_toc(tocspec)

    # Guess the start page if provided
    if first_page:
        base, ext = os.path.splitext(first_page)
        if ext[1:] == 'textile':
            first_page = base+os.extsep+'html'

    # Build the CHM project XML
    compressor.generate_hfp(first_page, title)

    # Finally, build the CHM file
    compressor.chmcmd()
    
    if specified_chmfile is not None:
        print('CHM file should be located at ' + specified_chmfile)
    else:
        print('CHM file should be located at ' + os.path.join(dest, chmfile))
