"""The preshrunk-cotton, or pscotton, module is used for processing
textile documents into a compressed HTML Help, or CHM, file.  The
CHM format is a popular online documentation format on Microsoft
Windows and compatible platforms.  

The module relies on the 'chmcmd' program supplied with Free Pascal.
'chmcmd' is a purely open-source CHM generator, acting as an
alternative to Microsoft's HTML Help Workshop.

The preshrunk-cotton module was written by Jeff Armstrong 
(jeff@approximatrix.com) of Approximatrix, LLC."""

from pscotton.textproc import *
from pscotton.chm import *
from pscotton.tt2chm import *