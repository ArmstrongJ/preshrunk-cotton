preshrunk-cotton
a module for processing textile markup to CHM files

by Jeffrey Armstrong <jeffrey.armstrong@approximatrix.com>
Copyright (C) 2010, 2011 Approximatrix, LLC

Introduction
============

preshrunk-cotton is a simple Python package for converting a 
directory tree containing textile files (along with images,
html, etc.), batch converting all textile, and compressing the
resultant directory tree into a Compressed HTML Help (CHM)
file.  

Requirements
============

Python 2.6+ or 3.1+
Python textile package     (http://pypi.python.org/pypi/textile)
Free Pascal's chmcmd tool  (http://freepascal.org/)

Please note that preshrunk-cotton does _not_ require the
Microsoft HTML Help Workshop.  It relies instead on chmcmd,
an open-source CHM file generator.

More information is available at:

	http://wiki.freepascal.org/chm
	
Usage
=====

= Source Directory = 

Some notes are necessary concerning the source directory
definition.  The source directory should contain files
written in the textile markup language designated by the
".textile" extension.  

The table of contents for the help file is constructed based
on the directory structure itself.  To label a "chapter" of
the help file, which would effectively be a directory,
include a file in the directory by the name "title.txt".
This file should contain a single line which should be the
desired "chapter" title for the current directory.  Page
entries in the table of contents are automatically generated
from the titles of the resultant HTML files.

= Deviation from Strict Textile = 

Textile does not specify markup components for making web
pages.  Rather, the textile markup is used to generate html.
To create a complete web page, the results of processing a 
textile text file through the Python textile module is
prepended with "<html>" and "<body>" tags, and their
equivalent closing tags are appended to the end of the
textile output.  

Page titles are determined from the first occurrence of an
"h1." markup.  For example, if a textile begins with:

	h1. Welcome
	
The resultant html file will contain the following:

	<title>Welcome</title>
	
The decision to use the first heading as a title is 
arbitrary.

= For Developers =

preshrunk-cotton contains two basic classes:

  TextileProcessor: Class for controlling the conversion of
                    a textile-containing directory tree
                    into an equivalent html-containing
                    directory tree
                    
  CHMProject: Class for generating the necessary XML and
              HTML input files and calling chmcmd.
              
Either is accessible simply by importing the pscotton module.

A convenience function is also provided, tt2chm, which can
automate the process of converting a textile-containing
directory tree into a CHM file.

= For Users =

The preshrunk-cotton package comes with a script, tt2chm,
that effectively implements the tt2chm function from the
command line.  The usage is simply:

	tt2chm <input directory> <output directory>
	
The resultant CHM file will be located in the top level of
the output directory.

Source Code
===========

The source code for preshrunk-cotton is available at:

	http://code.google.com/p/preshrunk-cotton/
	
The authors are listed in AUTHORS.

Copyright
=========

Copyright (C) 2010, 2011 Approximatrix, LLC
http://approximatrix.com/
http://code.google.com/p/preshrunk-cotton/

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

A full version of the license terms is available in LICENSE.

