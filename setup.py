# preshrunk-cotton
# Copyright (C) 2010, 2011 Approximatrix, LLC
# http://code.google.com/p/preshrunk-cotton/
# http://approximatrix.com/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# A full version of the license terms is available in LICENSE.

from setuptools import setup
    

import os.path

setup(name='preshrunk-cotton',
      version='0.4',
      description="preshrunk-cotton - a textile-to-CHM processing package using FPC's chmcmd",
      long_description="""preshrunk-cotton is a package for creating Compressed HTML Help (CHM) files
      from directory structures containing text files written using the Textile markup language.  The
      package converts all textile files to html before building the CHM file. CHM files, a popular
      help format on Microsoft Windows, are constructed by calling Free Pascal's 'chmcmd' executable,
      which is not included.  The 'chmcmd' tool is available with both Free Pascal and the related
      Lazarus IDE.  No closed-source programs or libraries are used.""",
      author='Jeffrey Armstrong',
      author_email='jeffrey.armstrong@approximatrix.com',
      url='http://approximatrix.com',
      packages=['pscotton'],
      package_dir={'pscotton':os.path.join('src','pscotton')},
      platforms = ["any"],
      scripts=[os.path.join('scripts','tt2chm'),os.path.join('scripts','tt2chm.cmd')],
      license='GPL',
      classifiers=[
          "License :: OSI Approved :: GNU General Public License (GPL)",
          "Programming Language :: Python",
          "Development Status :: 4 - Beta",
          "Intended Audience :: Developers",
          "Topic :: Documentation",
          "Topic :: Text Processing",
          "Environment :: Console",
      ],
      keywords='chm winhelp textile',
      install_requires=[
        'setuptools',
        'textile',]
     )
